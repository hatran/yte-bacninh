var dataphongkham = [
 {
   "STT": "2",
   "Name": "Phòng chẩn trị Y học cổ truyền tư nhân BẢO KIỆN",
   "address": "Số 85, Đường Điện Biên Phủ, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.285205",
   "Latitude": "105.725405"
 },
 {
   "STT": "4",
   "Name": "Phòng khám chuyên khoa Mắt tư nhân",
   "address": "Số 160, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28724409999999",
   "Latitude": "105.723288"
 },
 {
   "STT": "6",
   "Name": "Phòng Xét nghiệm Y khoa tư nhân.",
   "address": "Số 206, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.285972",
   "Latitude": "105.7215666"
 },
 {
   "STT": "7",
   "Name": "Phòng khám chuyên khoa VLTL,PHCN tư nhân",
   "address": "Số 466, Đường Võ Thị Sáu, Khóm 6, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "8",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 38, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "9",
   "Name": "Phòng khám chuyên khoa Nội + Siêu âm tổng quát tư nhân",
   "address": "Số 192, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "10",
   "Name": "Cơ sở Dịch vụ y tế tư nhân",
   "address": "Số 183/2, Đường Trần Phú, Khóm 4, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28605",
   "Latitude": "105.723316"
 },
 {
   "STT": "11",
   "Name": "Phòng khám chuyên khoa phụ sản , KHHGĐ",
   "address": "Số 204, Đường 23/8, Khóm 1, Phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2957307",
   "Latitude": "105.710112"
 },
 {
   "STT": "12",
   "Name": "Phòng chẩn trị Y học cổ truyền Phước Thiện",
   "address": "Số 163, Đường Điện Biên Phủ, Khóm 3, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2848429",
   "Latitude": "105.7250134"
 },
 {
   "STT": "13",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân.",
   "address": "Số 57 A, Đường Hai Bà Trưng, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2864051",
   "Latitude": "105.7249871"
 },
 {
   "STT": "14",
   "Name": "Phòng khám chuyên khoa Chẩn đoán hình ảnh tư nhân",
   "address": "Số 325, Đường Võ Thị Sáu, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2915311",
   "Latitude": "105.7123643"
 },
 {
   "STT": "15",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 141, Đường Cách Mạng, Phường 1, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2957663",
   "Latitude": "105.7363325"
 },
 {
   "STT": "16",
   "Name": "Phòng khám chuyên khoa Tâm thần kinh tư nhân",
   "address": "Số 100, Đường Hai Bà Trưng, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2870598",
   "Latitude": "105.7265504"
 },
 {
   "STT": "17",
   "Name": "Phòng khám chuyên khoa Tai Mũi Họng tư nhân",
   "address": "Số 484, Đường Võ Thị Sáu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28564409999999",
   "Latitude": "105.7211244"
 },
 {
   "STT": "18",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 08, Đường Minh Diệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28629529999999",
   "Latitude": "105.7270467"
 },
 {
   "STT": "19",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 32A, Đường Trần Phú, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2877184",
   "Latitude": "105.7222497"
 },
 {
   "STT": "20",
   "Name": "Phòng khám chuyên khoa Răng,Hàm,Mặt tư nhân",
   "address": "Số 657, Đường Trần Phú, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.30758569999999",
   "Latitude": "105.719462"
 },
 {
   "STT": "21",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân.",
   "address": "Số 30, Đường Hòa Bình, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2891669",
   "Latitude": "105.7244279"
 },
 {
   "STT": "22",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân.",
   "address": "Số 30, Đường Hòa Bình, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2891669",
   "Latitude": "105.7244279"
 },
 {
   "STT": "23",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân",
   "address": "Lô 18B, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "24",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân Xuân Nga",
   "address": "Số 35A, Đường Võ Thị Sáu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "25",
   "Name": "Phòng khám chuyên khoa Nội tư nhân.",
   "address": "Số 53, Đường Trần Phú, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2886247",
   "Latitude": "105.7219672"
 },
 {
   "STT": "26",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân.",
   "address": "Số 175, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "27",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 235, Đường Trần Phú, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2930519",
   "Latitude": "105.7217233"
 },
 {
   "STT": "28",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 153B/5, Đường Tôn Đức Thắng, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2997251",
   "Latitude": "105.724448"
 },
 {
   "STT": "29",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân",
   "address": "Số 153B/5, Đường Tôn Đức Thắng, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2997251",
   "Latitude": "105.724448"
 },
 {
   "STT": "30",
   "Name": "Phòng khám chuyên khoa Tâm thần kinh tư nhân",
   "address": "Số 16 Bis, Đường Phan Đình Phùng, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2890058",
   "Latitude": "105.7255769"
 },
 {
   "STT": "31",
   "Name": "Phòng khám chuyên khoa Lao & Bệnh phổi tư nhân",
   "address": "Số 30, Đường Hà Huy Tập, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2868248",
   "Latitude": "105.7245575"
 },
 {
   "STT": "32",
   "Name": "Phòng khám chuyên khoa Lao & Bệnh phổi tư nhân",
   "address": "Số 432, Đường Võ Thị Sáu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2915311",
   "Latitude": "105.7123643"
 },
 {
   "STT": "33",
   "Name": "Phòng khám chuyên khoa Chẩn đoán hình ảnh (Siêu âm) tư nhân",
   "address": "Số 194, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "34",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 74, Đường Hòa Bình , Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28800429999999",
   "Latitude": "105.7221896"
 },
 {
   "STT": "35",
   "Name": "Cơ sở dịch vụ Kính thuốc tư nhân",
   "address": "Số 3B/6, Đường Trần Phú , Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.3042842",
   "Latitude": "105.7209152"
 },
 {
   "STT": "36",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 61, Đường Cao Văn Lầu, Phường 5, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2697688",
   "Latitude": "105.7298681"
 },
 {
   "STT": "37",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 176, Đường Hòa Bình, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28949849999999",
   "Latitude": "105.7242763"
 },
 {
   "STT": "38",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân",
   "address": "Số 51A, Đường Lý Thường Kiệt, Phường 3,thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2889538",
   "Latitude": "105.7251585"
 },
 {
   "STT": "39",
   "Name": "Phòng khám chuyên khoa Da liễu tư nhân",
   "address": "Số 346, Đường Võ Thị Sáu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2915311",
   "Latitude": "105.7123643"
 },
 {
   "STT": "40",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân",
   "address": ",Số 229, Đường 23 Tháng 8, Phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2960704",
   "Latitude": "105.7066689"
 },
 {
   "STT": "41",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 221, Đường 23 Tháng 8, Khóm 2, Phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2959838",
   "Latitude": "105.7079611"
 },
 {
   "STT": "42",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân",
   "address": "Số 2/46, Tỉnh lộ 38, Khóm 5, Phường 5, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2061818",
   "Latitude": "105.7348179"
 },
 {
   "STT": "43",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân",
   "address": "Số 472, Đường Võ Thị Sáu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "45",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân",
   "address": "Số 52A, Đường Lý Thường Kiệt, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28835709999999",
   "Latitude": "105.7242927"
 },
 {
   "STT": "46",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân",
   "address": "Số 97, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "47",
   "Name": "Phòng khám chuyên khoa Tai Mũi Họng tư nhân",
   "address": "Số 97, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "48",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân.",
   "address": "Số 47, Đường Hoàng Văn Thụ, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28689949999999",
   "Latitude": "105.7273218"
 },
 {
   "STT": "49",
   "Name": "Phòng khám chuyên khoa Mắt tư nhân",
   "address": "Số 345, Đường Võ Thị Sáu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2915311",
   "Latitude": "105.7123643"
 },
 {
   "STT": "50",
   "Name": "Phòng khám chuyên khoa Mắt tư nhân",
   "address": "Lô 10, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859037",
   "Latitude": "105.7219374"
 },
 {
   "STT": "51",
   "Name": "Phòng khám chuyên khoa Tâm thần kinh tư nhân",
   "address": "Số 54A/5, Đường Tôn Đức Thắng, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2997251",
   "Latitude": "105.724448"
 },
 {
   "STT": "52",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 31, Đường Trần Phú, Khóm 2, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.287709",
   "Latitude": "105.7222578"
 },
 {
   "STT": "53",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 188, Đường Hòa Bình, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2896746",
   "Latitude": "105.7259807"
 },
 {
   "STT": "54",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân",
   "address": "Số 240 (Số cũ 038), Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "55",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân",
   "address": "Số 426, Đường Võ Thị Sáu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2915311",
   "Latitude": "105.7123643"
 },
 {
   "STT": "56",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 222, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2854189",
   "Latitude": "105.7210407"
 },
 {
   "STT": "57",
   "Name": "Cơ sở Dịch vụ y tế tư nhân.",
   "address": "Số 409/1, Đường Cao Văn Lầu, Khóm Đầu Lộ, Phường Nhà Mát, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2314464",
   "Latitude": "105.7292491"
 },
 {
   "STT": "58",
   "Name": "Cơ sở Dịch vụ y tế tư nhân",
   "address": "Số 26, Đường Điện Biên Phủ, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2848429",
   "Latitude": "105.7250134"
 },
 {
   "STT": "59",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 20/15, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "60",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 18/6, Đường Trần Phú, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.3042842",
   "Latitude": "105.7209152"
 },
 {
   "STT": "61",
   "Name": "Phòng khám chuyên khoa Da liễu tư nhân",
   "address": "Số 7/3, Đường Nguyễn Tất Thành, Khóm 3, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2922136",
   "Latitude": "105.7258132"
 },
 {
   "STT": "62",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 7/3, Đường Nguyễn Tất Thành, Khóm 3, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2922136",
   "Latitude": "105.7258132"
 },
 {
   "STT": "63",
   "Name": "Phòng khám chuyên khoa Mắt tư nhân",
   "address": "Số 354, Đường Võ Thị Sáu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2915311",
   "Latitude": "105.7123643"
 },
 {
   "STT": "64",
   "Name": "Phòng khám chuyên khoa Mắt tư nhân",
   "address": "Số 08, Đường Võ Thị Sáu, Phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2915311",
   "Latitude": "105.7123643"
 },
 {
   "STT": "65",
   "Name": "Phòng khám chuyên khoa Ngoại thần kinh tư nhân",
   "address": "Số 05, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875386",
   "Latitude": "105.7243465"
 },
 {
   "STT": "66",
   "Name": "Phòng khám chuyên khoa Chẩn đoán hình ảnh tư nhân",
   "address": "Số 127, Đường Võ Thị Sáu, Khóm 2, Phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2925449",
   "Latitude": "105.7122141"
 },
 {
   "STT": "67",
   "Name": "Cơ sở Dịch vụ y tế tư nhân",
   "address": "Số 372, Đường 23 Tháng 8, Khóm Trà Kha, Phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2967004",
   "Latitude": "105.7026486"
 },
 {
   "STT": "68",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 249, Đường 23 Tháng 8, Khóm Trà Kha, Phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2962232",
   "Latitude": "105.7059908"
 },
 {
   "STT": "69",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 25A, Đường Võ Thị Sáu, Khóm 1, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "70",
   "Name": "Cơ sở dịch vụ Kính thuốc tư nhân Quỳnh Nhu I",
   "address": "Số 433 (Số cũ 12), Đường Cách Mạng, Phường 1, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.3004123",
   "Latitude": "105.7303371"
 },
 {
   "STT": "71",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 10, Đường Đặng Thuỳ Trâm, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2845506",
   "Latitude": "105.7211785"
 },
 {
   "STT": "72",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân.",
   "address": "Số 69,71, Đường Văn Tiến Dũng, Khóm 10, Phường 1, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.3064918",
   "Latitude": "105.7347934"
 },
 {
   "STT": "73",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 474, Đường Võ Thị Sáu, Khóm 6, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "74",
   "Name": "Phòng khám chuyên khoa Răng hàm mặt tư nhân Toàn Ý.",
   "address": "Số 95/5, Đường Trần Phú, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2860397",
   "Latitude": "105.7233164"
 },
 {
   "STT": "75",
   "Name": "Phòng khám chuyên khoa Ngoại , Ung bướu tư nhân",
   "address": "Số 242, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "76",
   "Name": "Phòng Xét nghiệm Y khoa tư nhân",
   "address": "Số 242, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "77",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 179 (Số cũ 32A), Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "78",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 133A/4, Quốc lộ 1A, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.297723",
   "Latitude": "105.7181608"
 },
 {
   "STT": "79",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 130B/1, Đường Cao Văn Lầu Phường Nhà Mát, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.23632059999999",
   "Latitude": "105.7337844"
 },
 {
   "STT": "80",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân",
   "address": "Số 144/4, Quốc lộ 1A, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.297723",
   "Latitude": "105.7181608"
 },
 {
   "STT": "81",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân",
   "address": "Số 144/4, Quốc lộ 1A, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.297723",
   "Latitude": "105.7181608"
 },
 {
   "STT": "82",
   "Name": "Phòng Xét nghiệm Y khoa tư nhân",
   "address": "Số 66, Đường Võ Thị Sáu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859558",
   "Latitude": "105.7211284"
 },
 {
   "STT": "83",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân",
   "address": "Số 176, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "84",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 194, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "85",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân.",
   "address": "Số 2A/2, Đường Hòa Bình, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2876759",
   "Latitude": "105.7211514"
 },
 {
   "STT": "86",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 127, Đường Võ Thị Sáu, Phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2915311",
   "Latitude": "105.7123643"
 },
 {
   "STT": "87",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân",
   "address": "F1, 22, Đường Nguyễn Trung Trực, Phường 5, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2785841",
   "Latitude": "105.7311573"
 },
 {
   "STT": "88",
   "Name": "Phòng khám chuyên khoa Tai Mũi Họng tư nhân",
   "address": "Số 265, Đường Nguyễn Thị Minh Khai, Phường 5, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.284295",
   "Latitude": "105.729086"
 },
 {
   "STT": "89",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": ", Số 13, Đường Hai Bà Trưng, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2870598",
   "Latitude": "105.7265504"
 },
 {
   "STT": "90",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 34, Đường Nguyễn Huệ , Khóm 5, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2838539",
   "Latitude": "105.7208581"
 },
 {
   "STT": "91",
   "Name": "Phòng khám chuyên khoa Tai , Mũi , Họng tư nhân",
   "address": "Số 36B/4, Đường Trần Huỳnh, Khóm 2, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2940842",
   "Latitude": "105.7216362"
 },
 {
   "STT": "92",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": ", Số 205, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "93",
   "Name": "Cơ sở Dịch vụ Y tế tư nhân",
   "address": "Số 27, Đường Lê Văn Duyệt, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28828019999999",
   "Latitude": "105.7247001"
 },
 {
   "STT": "94",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 1A, Đường Trần Phú, Khóm 2, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2860397",
   "Latitude": "105.7233164"
 },
 {
   "STT": "95",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 33 (Số cũ 44), Đường Hà Huy Tập, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2868248",
   "Latitude": "105.7245575"
 },
 {
   "STT": "96",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân",
   "address": "Số 175, Đường 23/8, Phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2950885",
   "Latitude": "105.7136748"
 },
 {
   "STT": "97",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 18/6, Đường Trần Phú, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.3042842",
   "Latitude": "105.7209152"
 },
 {
   "STT": "98",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân",
   "address": "Số 92, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "99",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân",
   "address": "Số 92, Đường Bà Triệu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "100",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân.",
   "address": "Số 155/6, Đường hòa Bình, Khóm 6, Phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2895633",
   "Latitude": "105.7253827"
 },
 {
   "STT": "101",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân",
   "address": "F1, 22, Đường Nguyễn Trung Trực, Phường 5, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2785841",
   "Latitude": "105.7311573"
 },
 {
   "STT": "102",
   "Name": "Phòng khám chuyên khoa Ngoại + Ung Bướu tư nhân",
   "address": "Số 258, Đường Võ Thị Sáu, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2915311",
   "Latitude": "105.7123643"
 },
 {
   "STT": "103",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt ANH TUẤN tư nhân",
   "address": "Số 21/14, Đường Võ Thị Sáu, Khóm 1, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "104",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 150A, Đường Hòa Bình nối dài, Phường 1, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2894082",
   "Latitude": "105.7252872"
 },
 {
   "STT": "105",
   "Name": "Phòng khám chuyên khoa Mắt tư nhân",
   "address": "Số 150A, Đường Hòa Bình nối dài, Phường 1, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2894082",
   "Latitude": "105.7252872"
 },
 {
   "STT": "106",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân Trần Như Nhàn.",
   "address": "Số 352, đường Võ Thị Sáu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "107",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân Quốc Sử",
   "address": "Số 45, đường Cao Văn Lầu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28319939999999",
   "Latitude": "105.7256553"
 },
 {
   "STT": "108",
   "Name": "Phòng khám Đa khoa tư nhân Nhân Hòa.",
   "address": "Số 79, F7, đường Trần Phú, phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2890223",
   "Latitude": "105.7217683"
 },
 {
   "STT": "109",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 181, đường Bà Triệu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "110",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân",
   "address": "Số 58, đường Hòa Bình, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2877197",
   "Latitude": "105.7214757"
 },
 {
   "STT": "111",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 28/9, đường Hòa Bình, phường 1, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.289235",
   "Latitude": "105.724508"
 },
 {
   "STT": "112",
   "Name": "Cơ sở dịch vụ Y tế tư nhân.",
   "address": "Số 93, đường Hoàng Văn Thụ, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2851994",
   "Latitude": "105.7239139"
 },
 {
   "STT": "113",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 35A, đường Võ Thị Sáu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "114",
   "Name": "Phòng chẩn trị Y học cổ truyền tư nhân.",
   "address": "Số 30, đường Đoàn Thị Điểm, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2843421",
   "Latitude": "105.7187176"
 },
 {
   "STT": "115",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân",
   "address": "Số 09, đường Ninh Bình, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2867426",
   "Latitude": "105.7266766"
 },
 {
   "STT": "116",
   "Name": "Phòng chẩn trị Y học cổ truyền , Hội Đông y BẮC NINH..",
   "address": "Số 74, đường Thống Nhất, phường 5, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.284749",
   "Latitude": "105.726883"
 },
 {
   "STT": "117",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân Trần Phước Đức.",
   "address": "Số 115, đường Võ Thị Sáu, khóm 2, phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2925449",
   "Latitude": "105.7122141"
 },
 {
   "STT": "118",
   "Name": "Cơ sở dịch vụ Y tế tư nhân Thuận Thành",
   "address": "Số 30, đường Hà Huy Tập, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2868248",
   "Latitude": "105.7245575"
 },
 {
   "STT": "119",
   "Name": "Phòng khám chuyên khoa phụ sản , KHHGĐ tư nhân",
   "address": "Số 95, đường Bà Triệu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 205, đường Bà Triệu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 177, đường Võ Thị Sáu, phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2903474",
   "Latitude": "105.7150501"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 221, đường 23/8, khóm 2, phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2959838",
   "Latitude": "105.7079611"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt Việt Mỹ tư nhân.",
   "address": "Số 119, đường Bà Triệu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt Quốc tế Sài Gòn tư nhân.",
   "address": "Số 134/4, đường Trần Phú, phường 7, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2959462",
   "Latitude": "105.7214469"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt Duy Tân tư nhân",
   "address": "Số 91 (Số cũ 18), đường Bà Triệu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "",
   "Name": "Phòng chẩn trị Y học cổ truyền tư nhân",
   "address": "Số 1, Lô F, đường Nguyễn Bỉnh Khiêm, phường 1, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.3004123",
   "Latitude": "105.7303371"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Chẩn đoán hình ảnh (Siêu âm) tư nhân.",
   "address": "Số 195, đường Bà Triệu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân",
   "address": "Số 6A, đường Bà Triệu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 440, đường 23/8, phường 8, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2967004",
   "Latitude": "105.7026486"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Nhà không số, đường Nguyễn Lương Bằng, phường 2, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.277382",
   "Latitude": "105.72377"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân",
   "address": "Số 34, Lô J, đường Lương Định Của, phường 1, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.3004123",
   "Latitude": "105.7303371"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân Xuân Nga 1",
   "address": "Số 38A, đường Bà Triệu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859088",
   "Latitude": "105.7214513"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ kính thuốc tư nhân",
   "address": "Số 09, đường Võ Thị Sáu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "",
   "Name": "Phòng X,Quang tư nhân.",
   "address": "Số 047, đường Võ Thị Sáu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "",
   "Name": "Phòng X,Quang tư nhân ngoài giờ.",
   "address": "C,14, đường Bà Triệu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28952159999999",
   "Latitude": "105.7271631"
 },
 {
   "STT": "",
   "Name": "Phòng X,Quang tư nhân ngoài giờ.",
   "address": "Số 192, đường Bà Triệu, phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.2859498",
   "Latitude": "105.7218432"
 },
 {
   "STT": "",
   "Name": "Phòng khám , Quản lý sức khỏe cán bộ trực thuộc Ban Bảo vệ, chăm sóc sức khỏe cán bộ Tỉnh ủy",
   "address": "Số 27, đường Lê Văn Duyệt, Phường 3, thành phố BẮC NINH, BẮC NINH",
   "Longtitude": "9.28828019999999",
   "Latitude": "105.7247001"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp B, Thị trấn Hòa Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.27588089999999",
   "Latitude": "105.6226976"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 323A, Đường Lộ Mới, Ấp 15,Xã Vĩnh Mỹ B,Hòa Bình, BẮC NINH",
   "Longtitude": "9.269175",
   "Latitude": "105.583428"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 169A, Ấp 18, Xã Vĩnh Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.3444221",
   "Latitude": "105.5608265"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 78, Ấp A, Thị trấn Hòa Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 12, Quốc lộ 1A, Ấp A1, Thị trấn Hòa Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ.",
   "address": "Số 09, Ấp 17, Xã Vĩnh Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.31152129999999",
   "Latitude": "105.5478205"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền .",
   "address": "Số 121, Ấp Thị trấn A, Thị trấn Hòa Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân ",
   "address": "Ấp Thị trấn B, Thị trấn Hòa Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân ",
   "address": "Số 212, Ấp B, Thị trấn Hòa Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp Bình Minh, Xã Vĩnh Mỹ B, Hòa Bình, BẮC NINH",
   "Longtitude": "9.27422769999999",
   "Latitude": "105.5584749"
 },
 {
   "STT": "",
   "Name": "Phòng chẩn trị Y học cổ truyền , Hội Đông Y Hòa Bình.",
   "address": "Hội Đông Y Hòa Bình Ấp Thị trấn A Thị trấn Hòa Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội ",
   "address": "Quốc lộ 1A, Ấp thị trấn A1, thị trấn Hòa Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2704595",
   "Latitude": "105.5897386"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội từ thiện Bửu An",
   "address": "Ấp An Khoa, Xã Vĩnh Mỹ B, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2765995",
   "Latitude": "105.5591615"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 507, ấp thị trấn B, thị trấn Hòa Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp Do Thới, Xã Vĩnh Mỹ A, Hòa Bình, BẮC NINH",
   "Longtitude": "9.23258549999999",
   "Latitude": "105.5826123"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân ",
   "address": "Ấp 14, Xã Vĩnh Mỹ B, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2765995",
   "Latitude": "105.5591615"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp 21, Xã Minh Diệu, Hòa Bình, BẮC NINH",
   "Longtitude": "9.3390177",
   "Latitude": "105.6060661"
 },
 {
   "STT": "",
   "Name": "Cơ sở dịch vụ y tế tư nhân ",
   "address": "Ấp Vĩnh Mẫu, Xã Vĩnh Hậu, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2131068",
   "Latitude": "105.6588485"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại , Chấn thương chỉnh hình tư nhân .",
   "address": "Ấp A, thị trấn Hòa Bình, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân Tri Ân Đường ",
   "address": "Ấp 15, Xã Vĩnh Mỹ B, Hòa Bình, BẮC NINH",
   "Longtitude": "9.2704595",
   "Latitude": "105.5897386"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân ",
   "address": "Số 141, Ấp Trà Ban II, Xã Châu Hưng A, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.37675679999999",
   "Latitude": "105.7233814"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 117, Ấp Cái Giá, Xã Hưng Hội, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.32475129999999",
   "Latitude": "105.7463948"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 58, Ấp Tam Hưng, Xã Vĩnh Hưng, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.390127",
   "Latitude": "105.598776"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 520, Ấp Trà Ban I, Xã Châu Hưng A, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.37675679999999",
   "Latitude": "105.7233814"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân .",
   "address": "Số 36A, Hương lộ 6, Ấp Cù Lao, Xã Hưng Hội , Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.3028015",
   "Latitude": "105.744701"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân .",
   "address": "Số 15 , Ấp Trà Ban I, Xã Châu Hưng A, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.37675679999999",
   "Latitude": "105.7233814"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế .",
   "address": "Số 196, Ấp Trà Ban II, Xã Châu Hưng A, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.37675679999999",
   "Latitude": "105.7233814"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 234, Ấp Trà Ban II, Xã Châu Hưng A, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.37675679999999",
   "Latitude": "105.7233814"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân .",
   "address": "Số 326 (Số cũ 236), Cái Dầy, Thị trấn Châu Hưng, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân ",
   "address": "Số 161, Ấp Cái Dầy, Thị trấn Châu Hưng, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": ", Số 48, Ấp Sóc Đồn, Xã Hưng Hội, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.3372716",
   "Latitude": "105.7644596"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp Xẻo Chích, thị trấn Châu Hưng, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.3333333",
   "Latitude": "105.7166667"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân ",
   "address": "Số 393, ấp Trà Ban II, Xã Châu Hưng A, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.37675679999999",
   "Latitude": "105.7233814"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 34, ấp Tam Hưng, Xã Vĩnh Hưng, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "",
   "Name": "Phòng chẩn trị Y học cổ truyền tư nhân ",
   "address": "Số 179, Quốc lộ 1A, ấp Xẻo Chích, thị trấn Châu Hưng, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.3388612",
   "Latitude": "105.7292491"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa phụ sản , KHHGĐ tư nhân .",
   "address": "Số 48, ấp Sóc Đồn, Xã Hưng Hội, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.3372716",
   "Latitude": "105.7644596"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 520, ấp Trà Ban I, Xã Châu Hưng A, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.37675679999999",
   "Latitude": "105.7233814"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân Thiên Phú .",
   "address": "Số 258, ấp Phước Thạnh 1, Xã Long Thạnh, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân .",
   "address": "Ấp Tam Hưng, Xã Vĩnh Hưng, Vĩnh Lợi, BẮC NINH",
   "Longtitude": "9.38741939999999",
   "Latitude": "105.609169"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân ",
   "address": "Số 73, Quốc Lộ 1A, Ấp 2, Thị trấn Hộ Phòng,Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám Đa khoa tư nhân Khải Hoàng.",
   "address": "Số 413, phường 1, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân",
   "address": "Số 251, Ấp 1, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.2420549",
   "Latitude": "105.4691899"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 351, Ấp 1, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.2420549",
   "Latitude": "105.4691899"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 140, Đường Thanh Niên, Ấp 1, Thị trấn Hộ Phòng, Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 177, Ấp Khúc Tréo B, Xã Tân Phong, Giá Rai, BẮC NINH",
   "Longtitude": "9.1989667",
   "Latitude": "105.3372637"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân ",
   "address": "Số 01, Ấp 1, Thị trấn Hộ Phòng, Giá Rai, BẮC NINH",
   "Longtitude": "9.2333333",
   "Latitude": "105.4166667"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 278, Quốc lộ 1A, Ấp 2, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.2388149",
   "Latitude": "105.4586323"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân ",
   "address": "Số 86/17, Quốc lộ 1A, Ấp 2, Thị trấn Hộ Phòng, Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa RHM tư nhân ",
   "address": "Số 132, Quốc lộ 1A, Ấp 5, Thị trấn Hộ Phòng, Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 449, Quốc lộ 1A, Ấp 2, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.2381298",
   "Latitude": "105.4549847"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 12, Quốc lộ 1A, Ấp Khúc Tréo B, Xã Tân Phong, Giá Rai, BẮC NINH",
   "Longtitude": "9.1963166",
   "Latitude": "105.3298112"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Da Liễu tư nhân .",
   "address": ", Số 14, Ấp I, Xã Phong Thạnh Đông A, Giá Rai, BẮC NINH",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân .",
   "address": "Số 386, Ấp Nhàn Dân A, Xã Tân Phong, Giá Rai, BẮC NINH",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân.",
   "address": "Số 273, Ấp 2, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.251623",
   "Latitude": "105.513886"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân .",
   "address": "Số 927, Ấp 2, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.2388149",
   "Latitude": "105.4586323"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 44, Cách mạng tháng Tám, Ấp 2, Thị trấn Hộ Phòng, Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân.",
   "address": "Số 230, Ấp I, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.2388149",
   "Latitude": "105.4586323"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân.",
   "address": "Ấp Xóm Mới, Xã Tân Thạnh, Giá Rai, BẮC NINH",
   "Longtitude": "9.17605599999999",
   "Latitude": "105.2794311"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp 2, Xã Phong Thạnh Đông A, Giá Rai, BẮC NINH",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 581/A, Ấp 2, Xã Phong Thạnh Đông A, Giá Rai, BẮC NINH",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân .",
   "address": "Ấp Kinh Lớn, Xã Tân Thạnh, Giá Rai, BẮC NINH",
   "Longtitude": "9.2090313",
   "Latitude": "105.2604409"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp 3, Xã Phong Thạnh Đông A, Giá Rai, BẮC NINH",
   "Longtitude": "9.2780956",
   "Latitude": "105.5042839"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Tai mũi họng tư nhân .",
   "address": "Khu tập thể UBND Giá Rai, Ấp I, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.23626799999999",
   "Latitude": "105.450451"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân .",
   "address": "Số 360, Ấp I, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.2388149",
   "Latitude": "105.4586323"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân.",
   "address": "Số 95, Ấp II, Thị trấn Hộ Phòng, Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân ",
   "address": "Số 334, Quốc lộ I, Ấp I, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.2381298",
   "Latitude": "105.4549847"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 325, Quốc lộ I, Ấp 2, Thị trấn Giá Rai, Giá Rai, BẮC NINH",
   "Longtitude": "9.2388149",
   "Latitude": "105.4586323"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 284, Ấp 2, Xã Phong Thạnh Đông A, Giá Rai, BẮC NINH",
   "Longtitude": "9.255232",
   "Latitude": "105.5173027"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội , Nhi tư nhân ",
   "address": "Ấp 19, Xã Phong Thạnh, Giá Rai, BẮC NINH",
   "Longtitude": "9.3116383",
   "Latitude": "105.393734"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân .",
   "address": "Số 128, ấp 5, thị trấn Hộ Phòng, Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân.",
   "address": "Số 387, ấp Nhàn Dân A, Xã Tân Phong, Giá Rai, BẮC NINH",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân .",
   "address": "Số 86/18, ấp 2, thị trấn Hộ Phòng, Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng chẩn trị Y học cổ truyền tư nhân .",
   "address": "Số 55, ấp Nhàn Dân A, Xã Tân Phong, Giá Rai, BẮC NINH",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân .",
   "address": "Số 196, ấp Nhàn Dân B, Xã Tân Phong, Giá Rai, BẮC NINH",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 124, Quốc lộ 1A, ấp I, Xã Tân Phong, huyện Giá Rai, BẮC NINH",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 08, ấp Khúc Tréo B, Xã Tân Phong, Giá Rai, BẮC NINH",
   "Longtitude": "9.1963166",
   "Latitude": "105.3298112"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân.",
   "address": "Ấp 2, Láng Tròn, Xã Phong Thạnh Đông A, Giá Rai, BẮC NINH",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân Sài Gòn.",
   "address": "Số 114, Quốc lộ 1A, thị trấn Hộ Phòng, Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân Tiến Phúc.",
   "address": "Số 100, Quốc lộIA, ấp 1, thị trấn Hộ Phòng, Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Ấp 19, Xã Phong Thạnh, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.3116383",
   "Latitude": "105.393734"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt Việt Mỹ 9 tư nhân.",
   "address": "Số 192, Quốc lộ 1A, ấp II, phường Hộ Phòng, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân.",
   "address": "Số 166, Quốc lộ IA, khóm 1, phường Hộ Phòng, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân.",
   "address": "Khóm 2, phường Hộ Phòng, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.2251694",
   "Latitude": "105.4074964"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân.",
   "address": "Số 398, Ấp Nhàn Dân A, Xã Tân Phong, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "",
   "Name": "Cơ sở dịch vụ y tế tư nhân ",
   "address": "Số 71, Quốc lộ IA, khóm 2, phường Hộ Phòng, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 414, đường Trần Hưng Đạo, khóm 5, phường 1, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "",
   "Name": "Cơ sở dịch vụ tiêm (chích), thay băng, đếm mạch, đo nhiệt độ, đo huyết áp . ",
   "address": "Khóm 2, phường Láng Tròn, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.251623",
   "Latitude": "105.513886"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân.",
   "address": "Số 435, ấp Khúc Tréo A, Xã Tân Phong, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.1986972",
   "Latitude": "105.3301616"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân .",
   "address": "Ấp 16, Xã Phong Tân, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.3247982",
   "Latitude": "105.4419546"
 },
 {
   "STT": "",
   "Name": "Phòng Xét nghiệm Y khoa tư nhân .",
   "address": "Số 166, Quốc lộIA, phường Hộ Phòng, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 375, Khóm 5, phường Hộ Phòng, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân .",
   "address": "Số 494, đường Phan Thanh Giản, khóm 5, phường 1, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân Mỹ Hưng .",
   "address": "Khóm 5, phường Hộ Phòng, thị Xã Giá Rai, BẮC NINH",
   "Longtitude": "9.23030129999999",
   "Latitude": "105.4273166"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp Bửu II, Xã Long Điền Đông, Đông Hải, BẮC NINH",
   "Longtitude": "9.1487573",
   "Latitude": "105.5357139"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp 4, Thị trấn Gành Hào, Đông Hải, BẮC NINH",
   "Longtitude": "9.0383639",
   "Latitude": "105.4199115"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 35, Ấp 2, Thị trấn Gành Hào, Đông Hải, BẮC NINH",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân ",
   "address": "Ấp III, Thị trấn Gành Hào, Đông Hải, BẮC NINH",
   "Longtitude": "9.026724",
   "Latitude": "105.424268"
 },
 {
   "STT": "",
   "Name": "Phòng chẩn trị Y học cổ truyền tư nhân Trung Hòa",
   "address": "Số 24, Ấp 2, Thị trấn Gành Hào, Đông Hải, BẮC NINH",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Chẩn đoán hình ảnh tư nhân (Siêu âm tổng quát) ",
   "address": "Ấp III, Thị trấn Gành Hào, Đông Hải, BẮC NINH",
   "Longtitude": "9.026724",
   "Latitude": "105.424268"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân Cao Thắng .",
   "address": "Số 349, Ấp 2, Thị trấn Gành Hào, Đông Hải, BẮC NINH",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 58, Ấp 3, Thị trấn Gành Hào, Đông Hải, BẮC NINH",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 231, Ấp 4, Thị trấn Gành Hào, Đông Hải, BẮC NINH",
   "Longtitude": "9.0383639",
   "Latitude": "105.4199115"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân Lan Vy .",
   "address": "Ấp 4, thị trấn Gành, Đông Hải, BẮC NINH",
   "Longtitude": "9.0383639",
   "Latitude": "105.4199115"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Ấp Bờ Cảng, Xã Điền Hải, Đông Hải, BẮC NINH",
   "Longtitude": "9.108406",
   "Latitude": "105.489159"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 412, ấp Diêm Điền, Xã Điền Hải, Đông Hải, BẮC NINH",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Ấp Thành Thưởng, Xã An Trạch, Đông Hải, BẮC NINH",
   "Longtitude": "9.1763456",
   "Latitude": "105.3950939"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân An Phước .",
   "address": "Ấp Lung Chim, Xã Định Thành, Đông Hải, BẮC NINH",
   "Longtitude": "9.117867",
   "Latitude": "105.308566"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân .",
   "address": "Ấp Bờ Cảng, Xã Điền Hải, Đông Hải, BẮC NINH",
   "Longtitude": "9.108406",
   "Latitude": "105.489159"
 },
 {
   "STT": "",
   "Name": "Phòng chẩn trị Y học cổ truyền tư nhân.",
   "address": "Ấp Diêm Điền, Xã Điền Hải, Đông Hải, BẮC NINH",
   "Longtitude": "9.1056847",
   "Latitude": "105.4919293"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 234B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 214B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Da Liễu tư nhân ",
   "address": "Số 261A, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Chẩn đoán hình ảnh tư nhân ",
   "address": "Số 261A, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 01D, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4369825",
   "Latitude": "105.4617578"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 06B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 118/B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 244B, Ấp Long Thành, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 60B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân ",
   "address": "Số 236, Ấp Long Thành, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Răng Hàm Mặt tư nhân Quốc tế Á Châu.",
   "address": "Ấp Long Hòa, thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4561846",
   "Latitude": "105.4655614"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 47, Ấp Long Hoà, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 201, Ấp Phước Tân, Xã Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân",
   "address": "Số 86B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 324B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp Long Đức, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4561846",
   "Latitude": "105.4655614"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 88B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền từ thiện.",
   "address": "Chùa Long Thành, Số 56, Ấp Phước Thọ, Xã Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.376294",
   "Latitude": "105.395696"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân.",
   "address": "Số 260B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Cơ sở dịch vụ Kính thuốc tư nhân Sài Gòn .",
   "address": "Số 277A, Ấp Long Thành, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân ",
   "address": "Số 18B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Tai Mũi Họng tư nhân ",
   "address": "Số 200 B, Ấp Long Thành, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân .",
   "address": "Số 194B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân .",
   "address": "Số 352B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Y học gia đình tư nhân .",
   "address": "Ấp 9B, Xã Phong Thạnh Tây B, Phước Long, BẮC NINH",
   "Longtitude": "9.332002",
   "Latitude": "105.2779983"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân .",
   "address": "Số 332B, Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 110A, Ấp Phước Thành, Xã Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp Long Thành, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Ấp Tường 2, Xã Hưng Phú, Phước Long, BẮC NINH",
   "Longtitude": "9.3584424",
   "Latitude": "105.5213516"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 35, Ấp Long Hậu, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4377778",
   "Latitude": "105.4630556"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân ",
   "address": "Số 69A, Ấp Long Hòa , Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4413412",
   "Latitude": "105.4649198"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Chẩn đoán hình ảnh (Siêu âm) tư nhân .",
   "address": "Số 352B, ấp Nội Ô, thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 156, ấp Vĩnh Hòa, Xã Vĩnh Thanh, Phước Long, BẮC NINH",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 306, ấp Nội Ô, thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân Long Bửu Đường ",
   "address": "Số 35, ấp Tường Thắng B, Xã Vĩnh Thanh, Phước Long, BẮC NINH",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân ",
   "address": "Số 35, Ấp Long Thành, thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 69, ấp 2B, Xã Phong Thạnh Tây A, Phước Long, BẮC NINH",
   "Longtitude": "9.3376512",
   "Latitude": "105.3692249"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân .",
   "address": "Ấp Nội Ô, Thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4478981",
   "Latitude": "105.4616474"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Chẩn đoán hình ảnh (Siêu âm) tư nhân .",
   "address": "Số 352B, ấp Nội Ô, thị trấn Phước Long, Phước Long, BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "",
   "Name": "Cơ sở Dịch vụ y tế tư nhân",
   "address": "Ấp Ninh Thạnh, Xã Ninh Quới A, Hồng Dân, BẮC NINH",
   "Longtitude": "9.4876569",
   "Latitude": "105.5203539"
 },
 {
   "STT": "",
   "Name": "Phòng chẩn trị Y học cổ truyền tư nhân.",
   "address": "Ấp Đầu Sấu Đông, Xã Lộc Ninh, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5197602",
   "Latitude": "105.4185227"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Nội Ô, Thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Nội Ô, Thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp Nội Ô, Thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "",
   "Name": "Phòng Chẩn trị Y học cổ truyền tư nhân",
   "address": "Số 34, Ấp Ninh Thạnh, Xã Ninh Quới A, Hồng Dân, BẮC NINH",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 248,Ấp Ninh Thạnh, Xã Ninh Quới A, Hồng Dân, BẮC NINH",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân ",
   "address": "Số 184, Ấp Kênh Xáng, Xã Lộc Ninh,Hồng Dân, BẮC NINH",
   "Longtitude": "9.5197602",
   "Latitude": "105.4185227"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Số 263, Ấp Ninh Phước, Xã Ninh Quới A, Hồng Dân, BẮC NINH",
   "Longtitude": "9.559736",
   "Latitude": "105.488828"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội + Siêu âm tổng quát + Đo điện tâm đồ.",
   "address": "Ấp Nội Ô, Thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp Đầu Sấu Đông, Xã Lộc Ninh, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5197602",
   "Latitude": "105.4185227"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp Nội Ô, Thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Phụ sản , KHHGĐ tư nhân ",
   "address": "thị trấnTM, Ấp Nội Ô, Thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân ",
   "address": "Ấp Ninh Phước, Xã Ninh Qưới A, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5575142",
   "Latitude": "105.4600843"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Đường Nguyễn Thị Minh Khai, Khu Trung tâm thương mại, Thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5643409",
   "Latitude": "105.4480554"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân .",
   "address": "Số 126, Khu IB, Ấp Nội Ô, Thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân .",
   "address": "Đường Bùi Thị Trường, Khu Trung tâm thương mại, Thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nhi tư nhân ",
   "address": "Ấp Kinh Xáng, Xã Lộc Ninh, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5599813",
   "Latitude": "105.4530714"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Ngoại tư nhân ",
   "address": "Ấp Đầu Sấu Đông, Xã Lộc Ninh, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5197602",
   "Latitude": "105.4185227"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Trung tâm Thương mại, thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5651618",
   "Latitude": "105.4498833"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 09, đường Nguyễn Thị Minh Khai, ấp Nội Ô, thị trấn Ngan Dừa, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "",
   "Name": "Phòng khám chuyên khoa Nội tư nhân .",
   "address": "Số 14, ấp Ninh Thạnh, Xã Ninh Quới A, Hồng Dân, BẮC NINH",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "",
   "Name": "Phòng chẩn trị Y học cổ truyền tư nhân.",
   "address": "Số 533, ấp Kinh Xáng, Xã Lộc Ninh, Hồng Dân, BẮC NINH",
   "Longtitude": "9.5197602",
   "Latitude": "105.4185227"
 },
 {
   "STT": "313",
   "Name": "Quầy thuốc  Thành Đại",
   "address": "ấp Bửu 2, xã Long Điền Đông, huyện Đông Hải, tỉnh BẮC NINH",
   "Longtitude": "",
   "Latitude": ""
 },
 {
   "STT": "314",
   "Name": "Quầy thuốc  Ngọc Lượng",
   "address": "số 88, ấp Thuận Điền, xã Long Điền Tây, huyện Đông Hải, tỉnh BẮC NINH",
   "Longtitude": "",
   "Latitude": ""
 },
 {
   "STT": "315",
   "Name": "Quầy thuốc  Thiện Phước",
   "address": "số 46, ấp Diêm Điền, xã Điền Hải, huyện Đông Hải, tỉnh BẮC NINH",
   "Longtitude": "",
   "Latitude": ""
 },
 {
   "STT": "316",
   "Name": "Quầy thuốc  Ngọc Hân",
   "address": "số 39, ấp Thạnh II, xã long Điền, huyện Đông Hải, tỉnh BẮC NINH",
   "Longtitude": "",
   "Latitude": ""
 },
 {
   "STT": "317",
   "Name": "Quầy thuốc  Mỹ Ảnh",
   "address": "ấp Ba mến, xã An Trạch A, huyện Đông Hải, tỉnh BẮC NINH",
   "Longtitude": "",
   "Latitude": ""
 },
 {
   "STT": "318",
   "Name": "Cơ sở bán buôn thuốc thành phẩm  Thái Nhựt",
   "address": "Số 07 Lầu 1, đường Tôn Đức Thắng, P. 1, thành phố BẮC NINH, tỉnh BẮC NINH",
   "Longtitude": "",
   "Latitude": ""
 },
 {
   "STT": "319",
   "Name": "Công ty TNHH Dược Phẩm Gia Nguyễn BẮC NINH",
   "address": "Số 26/129 đường Cao Văn Lầu, P. 2, thành phố BẮC NINH, tỉnh BẮC NINH",
   "Longtitude": "",
   "Latitude": ""
 },
 {
   "STT": "320",
   "Name": "Công ty Cổ Phần Dược Phẩm BẮC NINH",
   "address": "Số 99 Hoàng Văn Thụ, P. 3, thành phố BẮC NINH, tỉnh BẮC NINH",
   "Longtitude": "",
   "Latitude": ""
 },
 {
   "STT": "321",
   "Name": "Hiền Mai",
   "address": "Số 10-12 Hai Bà Trưng, P. 3, thành phố BẮC NINH, tỉnh BẮC NINH",
   "Longtitude": "",
   "Latitude": ""
 },
 {
   "STT": "322",
   "Name": "Chi nhánh Công ty cổ phần Dược Hậu Giang tại BẮC NINH",
   "address": "số 67, Nguyễn Thị Định, khóm 10, phường 1, thành phố BẮC NINH, tỉnh BẮC NINH",
   "Longtitude": "",
   "Latitude": ""
 }
];