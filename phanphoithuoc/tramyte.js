var dataptramyte = [
 {
   "STT": "1",
   "Name": "Trạm y tế Phường 1",
   "address": "Phường 1, Thị xã Giá Rai, Tỉnh BẮC NINH",
   "Longtitude": "9.237082",
   "Latitude": "105.453723"
 },
 {
   "STT": "2",
   "Name": "Trạm y tế Phường Hộ Phòng",
   "address": "Phường Hộ Phòng, Thị xã Giá Rai, Tỉnh BẮC NINH",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "3",
   "Name": "Trạm y tế Xã Phong Thạnh Đông",
   "address": "Xã Phong Thạnh Đông, Thị xã Giá Rai, Tỉnh BẮC NINH",
   "Longtitude": "9.31708699999999",
   "Latitude": "105.494688"
 },
 {
   "STT": "4",
   "Name": "Trạm y tế Phường Láng Tròn",
   "address": "Phường Láng Tròn, Thị xã Giá Rai, Tỉnh BẮC NINH",
   "Longtitude": "9.2558127",
   "Latitude": "105.5172505"
 },
 {
   "STT": "5",
   "Name": "Trạm y tế Xã Phong Tân",
   "address": "Xã Phong Tân, Thị xã Giá Rai, Tỉnh BẮC NINH",
   "Longtitude": "9.3247982",
   "Latitude": "105.4419546"
 },
 {
   "STT": "6",
   "Name": "Trạm y tế Xã Tân Phong",
   "address": "Xã Tân Phong, Thị xã Giá Rai, Tỉnh BẮC NINH",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "7",
   "Name": "Trạm y tế Xã Phong Thạnh",
   "address": "Xã Phong Thạnh, Thị xã Giá Rai, Tỉnh BẮC NINH",
   "Longtitude": "9.3053443",
   "Latitude": "105.3950939"
 },
 {
   "STT": "8",
   "Name": "Trạm y tế Xã Phong Thạnh A",
   "address": "Xã Phong Thạnh A, Thị xã Giá Rai, Tỉnh BẮC NINH",
   "Longtitude": "9.2612954",
   "Latitude": "105.4185227"
 },
 {
   "STT": "9",
   "Name": "Trạm y tế Xã Phong Thạnh Tây",
   "address": "Xã Phong Thạnh Tây, Thị xã Giá Rai, Tỉnh BẮC NINH",
   "Longtitude": "9.2653877",
   "Latitude": "105.3248269"
 },
 {
   "STT": "10",
   "Name": "Trạm y tế Xã Tân Thạnh",
   "address": "Xã Tân Thạnh, Thị xã Giá Rai, Tỉnh BẮC NINH",
   "Longtitude": "9.2090313",
   "Latitude": "105.2604409"
 },
 {
   "STT": "11",
   "Name": "Trạm y tế Phường 2",
   "address": "Phường 2, Thành phố BẮC NINH, Tỉnh BẮC NINH",
   "Longtitude": "9.2749201",
   "Latitude": "105.7175138"
 },
 {
   "STT": "12",
   "Name": "Trạm y tế Phường 3",
   "address": "Phường 3, Thành phố BẮC NINH, Tỉnh BẮC NINH",
   "Longtitude": "9.2861684",
   "Latitude": "105.721181"
 },
 {
   "STT": "13",
   "Name": "Trạm y tế Phường 5",
   "address": "Phường 5, Thành phố BẮC NINH, Tỉnh BẮC NINH",
   "Longtitude": "9.2738687",
   "Latitude": "105.7409852"
 },
 {
   "STT": "14",
   "Name": "Trạm y tế Phường 7",
   "address": "Phường 7, Thành phố BẮC NINH, Tỉnh BẮC NINH",
   "Longtitude": "9.297723",
   "Latitude": "105.7181608"
 },
 {
   "STT": "15",
   "Name": "Trạm y tế Phường 1",
   "address": "Phường 1, Thành phố BẮC NINH, Tỉnh BẮC NINH",
   "Longtitude": "9.3004123",
   "Latitude": "105.7303371"
 },
 {
   "STT": "16",
   "Name": "Trạm y tế Phường 8",
   "address": "Phường 8, Thành phố BẮC NINH, Tỉnh BẮC NINH",
   "Longtitude": "9.2974563",
   "Latitude": "105.6940454"
 },
 {
   "STT": "17",
   "Name": "Trạm y tế Phường Nhà Mát",
   "address": "Phường Nhà Mát, Thành phố BẮC NINH, Tỉnh BẮC NINH",
   "Longtitude": "9.2314464",
   "Latitude": "105.7292491"
 },
 {
   "STT": "18",
   "Name": "Trạm y tế Xã Vĩnh Trạch",
   "address": "Xã Vĩnh Trạch, Thành phố BẮC NINH, Tỉnh BẮC NINH",
   "Longtitude": "9.3090846",
   "Latitude": "105.7938069"
 },
 {
   "STT": "19",
   "Name": "Trạm y tế Xã Vĩnh Trạch Đông",
   "address": "Xã Vĩnh Trạch Đông, Thành phố BẮC NINH, Tỉnh BẮC NINH",
   "Longtitude": "9.2661286",
   "Latitude": "105.7938069"
 },
 {
   "STT": "20",
   "Name": "Trạm y tế Xã Hiệp Thành",
   "address": "Xã Hiệp Thành, Thành phố BẮC NINH, Tỉnh BẮC NINH",
   "Longtitude": "9.2303988",
   "Latitude": "105.7527221"
 },
 {
   "STT": "21",
   "Name": "Trạm y tế Xã Vĩnh Hưng",
   "address": "Xã Vĩnh Hưng, Huyện Vĩnh Lợi, Tỉnh BẮC NINH",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "22",
   "Name": "Trạm y tế Xã Vĩnh Hưng A",
   "address": "Xã Vĩnh Hưng A, Huyện Vĩnh Lợi, Tỉnh BẮC NINH",
   "Longtitude": "9.3995064",
   "Latitude": "105.5767493"
 },
 {
   "STT": "23",
   "Name": "Trạm y tế Thị trấn Châu Hưng",
   "address": "Thị trấn Châu Hưng, Huyện Vĩnh Lợi, Tỉnh BẮC NINH",
   "Longtitude": "9.3388612",
   "Latitude": "105.7292491"
 },
 {
   "STT": "24",
   "Name": "Trạm y tế Xã Châu Hưng A",
   "address": "Xã Châu Hưng A, Huyện Vĩnh Lợi, Tỉnh BẮC NINH",
   "Longtitude": "9.37675679999999",
   "Latitude": "105.7233814"
 },
 {
   "STT": "25",
   "Name": "Trạm y tế Xã Hưng Thành",
   "address": "Xã Hưng Thành, Huyện Vĩnh Lợi, Tỉnh BẮC NINH",
   "Longtitude": "9.3714248",
   "Latitude": "105.8407722"
 },
 {
   "STT": "26",
   "Name": "Trạm y tế Xã Hưng Hội",
   "address": "Xã Hưng Hội, Huyện Vĩnh Lợi, Tỉnh BẮC NINH",
   "Longtitude": "9.3372716",
   "Latitude": "105.7644596"
 },
 {
   "STT": "27",
   "Name": "Trạm y tế Xã Châu Thới",
   "address": "Xã Châu Thới, Huyện Vĩnh Lợi, Tỉnh BẮC NINH",
   "Longtitude": "9.358423",
   "Latitude": "105.652983"
 },
 {
   "STT": "28",
   "Name": "Trạm y tế Xã Long Thạnh",
   "address": "Xã Long Thạnh, Huyện Vĩnh Lợi, Tỉnh BẮC NINH",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "29",
   "Name": "Trạm y tế Thị trấn Phước Long",
   "address": "Thị trấn Phước Long, Huyện Phước Long, Tỉnh BẮC NINH",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "30",
   "Name": "Trạm y tế Xã Vĩnh Phú Đông",
   "address": "Xã Vĩnh Phú Đông, Huyện Phước Long, Tỉnh BẮC NINH",
   "Longtitude": "9.4293187",
   "Latitude": "105.5122694"
 },
 {
   "STT": "31",
   "Name": "Trạm y tế Xã Vĩnh Phú Tây",
   "address": "Xã Vĩnh Phú Tây, Huyện Phước Long, Tỉnh BẮC NINH",
   "Longtitude": "9.3678476",
   "Latitude": "105.4419546"
 },
 {
   "STT": "32",
   "Name": "Trạm y tế Xã Phước Long",
   "address": "Xã Phước Long, Huyện Phước Long, Tỉnh BẮC NINH",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "33",
   "Name": "Trạm y tế Xã Hưng Phú",
   "address": "Xã Hưng Phú, Huyện Phước Long, Tỉnh BẮC NINH",
   "Longtitude": "9.4113246",
   "Latitude": "105.5532993"
 },
 {
   "STT": "34",
   "Name": "Trạm y tế Xã Vĩnh Thanh",
   "address": "Xã Vĩnh Thanh, Huyện Phước Long, Tỉnh BẮC NINH",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "35",
   "Name": "Trạm y tế Xã Phong Thạnh Tây A",
   "address": "Xã Phong Thạnh Tây A, Huyện Phước Long, Tỉnh BẮC NINH",
   "Longtitude": "9.3376512",
   "Latitude": "105.3692249"
 },
 {
   "STT": "36",
   "Name": "Trạm y tế Xã Phong Thạnh Tây B",
   "address": "Xã Phong Thạnh Tây B, Huyện Phước Long, Tỉnh BẮC NINH",
   "Longtitude": "9.332002",
   "Latitude": "105.2779983"
 },
 {
   "STT": "37",
   "Name": "Trạm y tế Thị trấn Ngan Dừa",
   "address": "Thị trấn Ngan Dừa, Huyện Hồng Dân, Tỉnh BẮC NINH",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "38",
   "Name": "Trạm y tế Xã Ninh Quới",
   "address": "Xã Ninh Quới, Huyện Hồng Dân, Tỉnh BẮC NINH",
   "Longtitude": "9.55760499999999",
   "Latitude": "105.5357139"
 },
 {
   "STT": "39",
   "Name": "Trạm y tế Xã Ninh Quới A",
   "address": "Xã Ninh Quới A, Huyện Hồng Dân, Tỉnh BẮC NINH",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "40",
   "Name": "Trạm y tế Xã Ninh Hòa",
   "address": "Xã Ninh Hòa, Huyện Hồng Dân, Tỉnh BẮC NINH",
   "Longtitude": "9.559736",
   "Latitude": "105.488828"
 },
 {
   "STT": "41",
   "Name": "Trạm y tế Xã Lộc Ninh",
   "address": "Xã Lộc Ninh, Huyện Hồng Dân, Tỉnh BẮC NINH",
   "Longtitude": "9.5197602",
   "Latitude": "105.4185227"
 },
 {
   "STT": "42",
   "Name": "Trạm y tế Xã Vĩnh Lộc",
   "address": "Xã Vĩnh Lộc, Huyện Hồng Dân, Tỉnh BẮC NINH",
   "Longtitude": "9.5639773",
   "Latitude": "105.3950939"
 },
 {
   "STT": "43",
   "Name": "Trạm y tế Xã Vĩnh Lộc A",
   "address": "Xã Vĩnh Lộc A, Huyện Hồng Dân, Tỉnh BẮC NINH",
   "Longtitude": "9.5830744",
   "Latitude": "105.3306814"
 },
 {
   "STT": "44",
   "Name": "Trạm y tế Xã Ninh Thạnh Lợi A",
   "address": "Xã Ninh Thạnh Lợi A, Huyện Hồng Dân, Tỉnh BẮC NINH",
   "Longtitude": "9.4376772",
   "Latitude": "105.3248269"
 },
 {
   "STT": "45",
   "Name": "Trạm y tế Xã Ninh Thạnh Lợi",
   "address": "Xã Ninh Thạnh Lợi, Huyện Hồng Dân, Tỉnh BẮC NINH",
   "Longtitude": "9.50133679999999",
   "Latitude": "105.348246"
 },
 {
   "STT": "46",
   "Name": "Trạm y tế Thị trấn Hòa Bình",
   "address": "Thị trấn Hòa Bình, Huyện Hoà Bình, Tỉnh BẮC NINH",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "47",
   "Name": "Trạm y tế Xã Minh Diệu",
   "address": "Xã Minh Diệu, Huyện Hoà Bình, Tỉnh BẮC NINH",
   "Longtitude": "9.3390177",
   "Latitude": "105.6060661"
 },
 {
   "STT": "48",
   "Name": "Trạm y tế Xã Vĩnh Bình",
   "address": "Xã Vĩnh Bình, Huyện Hoà Bình, Tỉnh BẮC NINH",
   "Longtitude": "9.3411125",
   "Latitude": "105.5591615"
 },
 {
   "STT": "49",
   "Name": "Trạm y tế Xã Vĩnh Mỹ B",
   "address": "Xã Vĩnh Mỹ B, Huyện Hoà Bình, Tỉnh BẮC NINH",
   "Longtitude": "9.2765995",
   "Latitude": "105.5591615"
 },
 {
   "STT": "50",
   "Name": "Trạm y tế Xã Vĩnh Hậu",
   "address": "Xã Vĩnh Hậu, Huyện Hoà Bình, Tỉnh BẮC NINH",
   "Longtitude": "9.2131068",
   "Latitude": "105.6588485"
 },
 {
   "STT": "51",
   "Name": "Trạm y tế Xã Vĩnh Hậu A",
   "address": "Xã Vĩnh Hậu A, Huyện Hoà Bình, Tỉnh BẮC NINH",
   "Longtitude": "9.23301459999999",
   "Latitude": "105.6940454"
 },
 {
   "STT": "52",
   "Name": "Trạm y tế Xã Vĩnh Mỹ A",
   "address": "Xã Vĩnh Mỹ A, Huyện Hoà Bình, Tỉnh BẮC NINH",
   "Longtitude": "9.23258549999999",
   "Latitude": "105.5826123"
 },
 {
   "STT": "53",
   "Name": "Trạm y tế Xã Vĩnh Thịnh",
   "address": "Xã Vĩnh Thịnh, Huyện Hoà Bình, Tỉnh BẮC NINH",
   "Longtitude": "9.1886028",
   "Latitude": "105.6060661"
 },
 {
   "STT": "54",
   "Name": "Trạm y tế Thị trấn Gành Hào",
   "address": "Thị trấn Gành Hào, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "55",
   "Name": "Trạm y tế Xã Long Điền Đông",
   "address": "Xã Long Điền Đông, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.1487573",
   "Latitude": "105.5357139"
 },
 {
   "STT": "56",
   "Name": "Trạm y tế Xã Long Điền Đông A",
   "address": "Xã Long Điền Đông A, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.1487573",
   "Latitude": "105.5357139"
 },
 {
   "STT": "57",
   "Name": "Trạm y tế Xã Long Điền",
   "address": "Xã Long Điền, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.1676975",
   "Latitude": "105.4481035"
 },
 {
   "STT": "58",
   "Name": "Trạm y tế Xã Long Điền Tây",
   "address": "Xã Long Điền Tây, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.0894582",
   "Latitude": "105.4185227"
 },
 {
   "STT": "59",
   "Name": "Trạm y tế Xã Điền Hải",
   "address": "Xã Điền Hải, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "60",
   "Name": "Trạm y tế Xã An Trạch",
   "address": "Xã An Trạch, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.157899",
   "Latitude": "105.3248269"
 },
 {
   "STT": "61",
   "Name": "Trạm y tế Xã An Trạch A",
   "address": "Xã An Trạch A, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.1763456",
   "Latitude": "105.3950939"
 },
 {
   "STT": "62",
   "Name": "Trạm y tế Xã An Phúc",
   "address": "Xã An Phúc, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.091471",
   "Latitude": "105.3716684"
 },
 {
   "STT": "63",
   "Name": "Trạm y tế Xã Định Thành",
   "address": "Xã Định Thành, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.1323056",
   "Latitude": "105.2955575"
 },
 {
   "STT": "64",
   "Name": "Trạm y tế Xã Định Thành A",
   "address": "Xã Định Thành A, Huyện Đông Hải, Tỉnh BẮC NINH",
   "Longtitude": "9.1225686",
   "Latitude": "105.2721456"
 },
];