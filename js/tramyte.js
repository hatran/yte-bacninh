var dataptramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế Phường Vũ Ninh",
   "address": "Phường Vũ Ninh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1975347,
   "Latitude": 106.0728356
 },
 {
   "STT": 2,
   "Name": "Trạm y tế Phường Đáp Cầu",
   "address": "Phường Đáp Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.2026299,
   "Latitude": 106.0948815
 },
 {
   "STT": 3,
   "Name": "Trạm y tế Phường Thị Cầu",
   "address": "Phường Thị Cầu, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1969959,
   "Latitude": 106.0904471
 },
 {
   "STT": 4,
   "Name": "Trạm y tế Phường Kinh Bắc",
   "address": "Phường Kinh Bắc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1888172,
   "Latitude": 106.0638342
 },
 {
   "STT": 5,
   "Name": "Trạm y tế Phường Vệ An",
   "address": "Phường Vệ An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1828931,
   "Latitude": 106.0596092
 },
 {
   "STT": 6,
   "Name": "Trạm y tế Phường Tiền An",
   "address": "Phường Tiền An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1815277,
   "Latitude": 106.0662223
 },
 {
   "STT": 7,
   "Name": "Trạm y tế Phường Đại Phúc",
   "address": "Phường Đại Phúc, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1738006,
   "Latitude": 106.0787142
 },
 {
   "STT": 8,
   "Name": "Trạm y tế Phường Ninh Xá",
   "address": "Phường Ninh Xá, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1768169,
   "Latitude": 106.0625483
 },
 {
   "STT": 9,
   "Name": "Trạm y tế Phường Suối Hoa",
   "address": "Phường Suối Hoa, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1831983,
   "Latitude": 106.0698741
 },
 {
   "STT": 10,
   "Name": "Trạm y tế Phường Võ Cường",
   "address": "Phường Võ Cường, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1644992,
   "Latitude": 106.0552006
 },
 {
   "STT": 11,
   "Name": "Trạm y tế Xã Hòa Long",
   "address": "Xã Hòa Long, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.2167558,
   "Latitude": 106.0540757
 },
 {
   "STT": 12,
   "Name": "Trạm y tế Phường Vạn An",
   "address": "Phường Vạn An, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.194997,
   "Latitude": 106.0490265
 },
 {
   "STT": 13,
   "Name": "Trạm y tế Phường Khúc Xuyên",
   "address": "Phường Khúc Xuyên, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1830123,
   "Latitude": 106.0434449
 },
 {
   "STT": 14,
   "Name": "Trạm y tế Phường Phong Khê",
   "address": "Phường Phong Khê, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1725674,
   "Latitude": 106.0316898
 },
 {
   "STT": 15,
   "Name": "Trạm y tế Xã Kim Chân",
   "address": "Xã Kim Chân, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941046,
   "Latitude": 106.1081103
 },
 {
   "STT": 16,
   "Name": "Trạm y tế Phường Vân Dương",
   "address": "Phường Vân Dương, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1605068,
   "Latitude": 106.0963513
 },
 {
   "STT": 17,
   "Name": "Trạm y tế Xã Nam Sơn",
   "address": "Xã Nam Sơn, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1405263,
   "Latitude": 106.0934117
 },
 {
   "STT": 18,
   "Name": "Trạm y tế Phường Khắc Niệm",
   "address": "Phường Khắc Niệm, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1314843,
   "Latitude": 106.0580066
 },
 {
   "STT": 19,
   "Name": "Trạm y tế Phường Hạp Lĩnh",
   "address": "Phường Hạp Lĩnh, Thành phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1309568,
   "Latitude": 106.0761376
 },
 {
   "STT": 20,
   "Name": "Trạm y tế Thị trấn Chờ",
   "address": "Thị trấn Chờ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1978889,
   "Latitude": 105.9494248
 },
 {
   "STT": 21,
   "Name": "Trạm y tế Xã Dũng Liệt",
   "address": "Xã Dũng Liệt, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2469267,
   "Latitude": 105.9957504
 },
 {
   "STT": 22,
   "Name": "Trạm y tế Xã Tam Đa",
   "address": "Xã Tam Đa, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2235133,
   "Latitude": 106.0321949
 },
 {
   "STT": 23,
   "Name": "Trạm y tế Xã Tam Giang",
   "address": "Xã Tam Giang, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2271932,
   "Latitude": 105.949194
 },
 {
   "STT": 24,
   "Name": "Trạm y tế Xã Yên Trung",
   "address": "Xã Yên Trung, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2202724,
   "Latitude": 105.9876149
 },
 {
   "STT": 25,
   "Name": "Trạm y tế Xã Thụy Hòa",
   "address": "Xã Thụy Hòa, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 26,
   "Name": "Trạm y tế Xã Hòa Tiến",
   "address": "Xã Hòa Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1941429,
   "Latitude": 105.9582372
 },
 {
   "STT": 27,
   "Name": "Trạm y tế Xã Đông Tiến",
   "address": "Xã Đông Tiến, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 28,
   "Name": "Trạm y tế Xã Yên Phụ",
   "address": "Xã Yên Phụ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1943571,
   "Latitude": 105.9259271
 },
 {
   "STT": 29,
   "Name": "Trạm y tế Xã Trung Nghĩa",
   "address": "Xã Trung Nghĩa, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1788105,
   "Latitude": 105.9670501
 },
 {
   "STT": 30,
   "Name": "Trạm y tế Xã Đông Phong",
   "address": "Xã Đông Phong, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.2206467,
   "Latitude": 105.9708806
 },
 {
   "STT": 31,
   "Name": "Trạm y tế Xã Long Châu",
   "address": "Xã Long Châu, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1919357,
   "Latitude": 105.9863674
 },
 {
   "STT": 32,
   "Name": "Trạm y tế Xã Văn Môn",
   "address": "Xã Văn Môn, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1697264,
   "Latitude": 105.9305567
 },
 {
   "STT": 33,
   "Name": "Trạm y tế Xã Đông Thọ",
   "address": "Xã Đông Thọ, Huyện Yên Phong, Tỉnh Bắc Ninh",
   "Longtitude": 21.1701536,
   "Latitude": 105.9560665
 },
 {
   "STT": 34,
   "Name": "Trạm y tế Thị trấn Phố Mới",
   "address": "Thị trấn Phố Mới, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1534803,
   "Latitude": 106.1536828
 },
 {
   "STT": 35,
   "Name": "Trạm y tế Xã Việt Thống",
   "address": "Xã Việt Thống, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1949254,
   "Latitude": 106.1288912
 },
 {
   "STT": 36,
   "Name": "Trạm y tế Xã Đại Xuân",
   "address": "Xã Đại Xuân, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1865968,
   "Latitude": 106.12575
 },
 {
   "STT": 37,
   "Name": "Trạm y tế Xã Nhân Hòa",
   "address": "Xã Nhân Hòa, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1826089,
   "Latitude": 106.1418799
 },
 {
   "STT": 38,
   "Name": "Trạm y tế Xã Bằng An",
   "address": "Xã Bằng An, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1715775,
   "Latitude": 106.1610343
 },
 {
   "STT": 39,
   "Name": "Trạm y tế Xã Phương Liễu",
   "address": "Xã Phương Liễu, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.163438,
   "Latitude": 106.12575
 },
 {
   "STT": 40,
   "Name": "Trạm y tế Xã Quế Tân",
   "address": "Xã Quế Tân, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1672864,
   "Latitude": 106.1901771
 },
 {
   "STT": 41,
   "Name": "Trạm y tế Xã Phù Lương",
   "address": "Xã Phù Lương, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1533716,
   "Latitude": 106.199266
 },
 {
   "STT": 42,
   "Name": "Trạm y tế Xã Phù Lãng",
   "address": "Xã Phù Lãng, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1603374,
   "Latitude": 106.2463305
 },
 {
   "STT": 43,
   "Name": "Trạm y tế Xã Phượng Mao",
   "address": "Xã Phượng Mao, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.150714,
   "Latitude": 106.1375107
 },
 {
   "STT": 44,
   "Name": "Trạm y tế Xã Việt Hùng",
   "address": "Xã Việt Hùng, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1440932,
   "Latitude": 106.1757379
 },
 {
   "STT": 45,
   "Name": "Trạm y tế Xã Ngọc Xá",
   "address": "Xã Ngọc Xá, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1424986,
   "Latitude": 106.2162425
 },
 {
   "STT": 46,
   "Name": "Trạm y tế Xã Châu Phong",
   "address": "Xã Châu Phong, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1360355,
   "Latitude": 106.2580983
 },
 {
   "STT": 47,
   "Name": "Trạm y tế Xã Bồng Lai",
   "address": "Xã Bồng Lai, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1239977,
   "Latitude": 106.166532
 },
 {
   "STT": 48,
   "Name": "Trạm y tế Xã Cách Bi",
   "address": "Xã Cách Bi, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1177583,
   "Latitude": 106.1786787
 },
 {
   "STT": 49,
   "Name": "Trạm y tế Xã Đào Viên",
   "address": "Xã Đào Viên, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1148879,
   "Latitude": 106.2080897
 },
 {
   "STT": 50,
   "Name": "Trạm y tế Xã Yên Giả",
   "address": "Xã Yên Giả, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1191679,
   "Latitude": 106.121473
 },
 {
   "STT": 51,
   "Name": "Trạm y tế Xã Mộ Đạo",
   "address": "Xã Mộ Đạo, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1174799,
   "Latitude": 106.145321
 },
 {
   "STT": 52,
   "Name": "Trạm y tế Xã Đức Long",
   "address": "Xã Đức Long, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.1221528,
   "Latitude": 106.2816361
 },
 {
   "STT": 53,
   "Name": "Trạm y tế Xã Chi Lăng",
   "address": "Xã Chi Lăng, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.0962004,
   "Latitude": 106.1343867
 },
 {
   "STT": 54,
   "Name": "Trạm y tế Xã Hán Quảng",
   "address": "Xã Hán Quảng, Huyện Quế Võ, Tỉnh Bắc Ninh",
   "Longtitude": 21.138547,
   "Latitude": 106.181402
 },
 {
   "STT": 55,
   "Name": "Trạm y tế Thị trấn Lim",
   "address": "Thị trấn Lim, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.144745,
   "Latitude": 106.0199355
 },
 {
   "STT": 56,
   "Name": "Trạm y tế Xã Phú Lâm",
   "address": "Xã Phú Lâm, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1727961,
   "Latitude": 105.9993672
 },
 {
   "STT": 57,
   "Name": "Trạm y tế Xã Nội Duệ",
   "address": "Xã Nội Duệ, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1400879,
   "Latitude": 106.0081819
 },
 {
   "STT": 58,
   "Name": "Trạm y tế Xã Liên Bão",
   "address": "Xã Liên Bão, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1293608,
   "Latitude": 106.0251321
 },
 {
   "STT": 59,
   "Name": "Trạm y tế Xã Hiên Vân",
   "address": "Xã Hiên Vân, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1228631,
   "Latitude": 106.050208
 },
 {
   "STT": 60,
   "Name": "Trạm y tế Xã Hoàn Sơn",
   "address": "Xã Hoàn Sơn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1122605,
   "Latitude": 105.9964291
 },
 {
   "STT": 61,
   "Name": "Trạm y tế Xã Lạc Vệ",
   "address": "Xã Lạc Vệ, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1185051,
   "Latitude": 106.0816536
 },
 {
   "STT": 62,
   "Name": "Trạm y tế Xã Việt Đoàn",
   "address": "Xã Việt Đoàn, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1178311,
   "Latitude": 106.0287512
 },
 {
   "STT": 63,
   "Name": "Trạm y tế Xã Phật Tích",
   "address": "Xã Phật Tích, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0965381,
   "Latitude": 106.0243433
 },
 {
   "STT": 64,
   "Name": "Trạm y tế Xã Tân Chi",
   "address": "Xã Tân Chi, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.1178311,
   "Latitude": 106.0287512
 },
 {
   "STT": 65,
   "Name": "Trạm y tế Xã Đại Đồng",
   "address": "Xã Đại Đồng, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0928361,
   "Latitude": 105.9876149
 },
 {
   "STT": 66,
   "Name": "Trạm y tế Xã Tri Phương",
   "address": "Xã Tri Phương, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0816438,
   "Latitude": 106.006533
 },
 {
   "STT": 67,
   "Name": "Trạm y tế Xã Minh Đạo",
   "address": "Xã Minh Đạo, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.08715,
   "Latitude": 106.0543885
 },
 {
   "STT": 68,
   "Name": "Trạm y tế Xã Cảnh Hưng",
   "address": "Xã Cảnh Hưng, Huyện Tiên Du, Tỉnh Bắc Ninh",
   "Longtitude": 21.0846853,
   "Latitude": 106.0302688
 },
 {
   "STT": 69,
   "Name": "Trạm y tế Phường Đông Ngàn",
   "address": "Phường Đông Ngàn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1156528,
   "Latitude": 105.9611748
 },
 {
   "STT": 70,
   "Name": "Trạm y tế Xã Tam Sơn",
   "address": "Xã Tam Sơn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1446421,
   "Latitude": 105.9746519
 },
 {
   "STT": 71,
   "Name": "Trạm y tế Xã Hương Mạc",
   "address": "Xã Hương Mạc, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1460011,
   "Latitude": 105.9452461
 },
 {
   "STT": 72,
   "Name": "Trạm y tế Xã Tương Giang",
   "address": "Xã Tương Giang, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1417883,
   "Latitude": 105.9905529
 },
 {
   "STT": 73,
   "Name": "Trạm y tế Xã Phù Khê",
   "address": "Xã Phù Khê, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1419508,
   "Latitude": 105.9333616
 },
 {
   "STT": 74,
   "Name": "Trạm y tế Phường Đồng Kỵ",
   "address": "Phường Đồng Kỵ, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1399547,
   "Latitude": 105.9494248
 },
 {
   "STT": 75,
   "Name": "Trạm y tế Phường Trang Hạ",
   "address": "Phường Trang Hạ, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1288596,
   "Latitude": 105.9519986
 },
 {
   "STT": 76,
   "Name": "Trạm y tế Phường Đồng Nguyên",
   "address": "Phường Đồng Nguyên, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1319011,
   "Latitude": 105.9729255
 },
 {
   "STT": 77,
   "Name": "Trạm y tế Phường Châu Khê",
   "address": "Phường Châu Khê, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1137324,
   "Latitude": 105.929323
 },
 {
   "STT": 78,
   "Name": "Trạm y tế Phường Tân Hồng",
   "address": "Phường Tân Hồng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.1087302,
   "Latitude": 105.9729255
 },
 {
   "STT": 79,
   "Name": "Trạm y tế Phường Đình Bảng",
   "address": "Phường Đình Bảng, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.0994022,
   "Latitude": 105.9494248
 },
 {
   "STT": 80,
   "Name": "Trạm y tế Xã Phù Chẩn",
   "address": "Xã Phù Chẩn, Thị xã Từ Sơn, Tỉnh Bắc Ninh",
   "Longtitude": 21.0855597,
   "Latitude": 105.9729255
 },
 {
   "STT": 81,
   "Name": "Trạm y tế Thị trấn Hồ",
   "address": "Thị trấn Hồ, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0568543,
   "Latitude": 106.0904721
 },
 {
   "STT": 82,
   "Name": "Trạm y tế Xã Hoài Thượng",
   "address": "Xã Hoài Thượng, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0818854,
   "Latitude": 106.1126129
 },
 {
   "STT": 83,
   "Name": "Trạm y tế Xã Đại Đồng Thành",
   "address": "Xã Đại Đồng Thành, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0732209,
   "Latitude": 106.0589899
 },
 {
   "STT": 84,
   "Name": "Trạm y tế Xã Mão Điền",
   "address": "Xã Mão Điền, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0636761,
   "Latitude": 106.1217707
 },
 {
   "STT": 85,
   "Name": "Trạm y tế Xã Song Hồ",
   "address": "Xã Song Hồ, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0568543,
   "Latitude": 106.0904721
 },
 {
   "STT": 86,
   "Name": "Trạm y tế Xã Đình Tổ",
   "address": "Xã Đình Tổ, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0625294,
   "Latitude": 106.0316898
 },
 {
   "STT": 87,
   "Name": "Trạm y tế Xã An Bình",
   "address": "Xã An Bình, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0473871,
   "Latitude": 106.118937
 },
 {
   "STT": 88,
   "Name": "Trạm y tế Xã Trí Quả",
   "address": "Xã Trí Quả, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0487351,
   "Latitude": 106.029068
 },
 {
   "STT": 89,
   "Name": "Trạm y tế Xã Gia Đông",
   "address": "Xã Gia Đông, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0385768,
   "Latitude": 106.0698963
 },
 {
   "STT": 90,
   "Name": "Trạm y tế Xã Thanh Khương",
   "address": "Xã Thanh Khương, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0437995,
   "Latitude": 106.0757749
 },
 {
   "STT": 91,
   "Name": "Trạm y tế Xã Trạm Lộ",
   "address": "Xã Trạm Lộ, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0314204,
   "Latitude": 106.11399
 },
 {
   "STT": 92,
   "Name": "Trạm y tế Xã Xuân Lâm",
   "address": "Xã Xuân Lâm, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0243424,
   "Latitude": 106.0194764
 },
 {
   "STT": 93,
   "Name": "Trạm y tế Xã Hà Mãn",
   "address": "Xã Hà Mãn, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0437995,
   "Latitude": 106.0757749
 },
 {
   "STT": 94,
   "Name": "Trạm y tế Xã Ngũ Thái",
   "address": "Xã Ngũ Thái, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0158673,
   "Latitude": 106.0329909
 },
 {
   "STT": 95,
   "Name": "Trạm y tế Xã Nguyệt Đức",
   "address": "Xã Nguyệt Đức, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0089094,
   "Latitude": 106.06609
 },
 {
   "STT": 96,
   "Name": "Trạm y tế Xã Ninh Xá",
   "address": "Xã Ninh Xá, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0157573,
   "Latitude": 106.0963513
 },
 {
   "STT": 97,
   "Name": "Trạm y tế Xã Nghĩa Đạo",
   "address": "Xã Nghĩa Đạo, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0097356,
   "Latitude": 106.1286901
 },
 {
   "STT": 98,
   "Name": "Trạm y tế Xã Song Liễu",
   "address": "Xã Song Liễu, Huyện Thuận Thành, Tỉnh Bắc Ninh",
   "Longtitude": 21.0063126,
   "Latitude": 106.0140586
 },
 {
   "STT": 99,
   "Name": "Trạm y tế Thị trấn Gia Bình",
   "address": "Thị trấn Gia Bình, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 100,
   "Name": "Trạm y tế Xã Vạn Ninh",
   "address": "Xã Vạn Ninh, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.1013155,
   "Latitude": 106.2580983
 },
 {
   "STT": 101,
   "Name": "Trạm y tế Xã Thái Bảo",
   "address": "Xã Thái Bảo, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.1094704,
   "Latitude": 106.238396
 },
 {
   "STT": 102,
   "Name": "Trạm y tế Xã Giang Sơn",
   "address": "Xã Giang Sơn, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0819441,
   "Latitude": 106.157072
 },
 {
   "STT": 103,
   "Name": "Trạm y tế Xã Cao Đức",
   "address": "Xã Cao Đức, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0874361,
   "Latitude": 106.2816361
 },
 {
   "STT": 104,
   "Name": "Trạm y tế Xã Đại Lai",
   "address": "Xã Đại Lai, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0905316,
   "Latitude": 106.2103471
 },
 {
   "STT": 105,
   "Name": "Trạm y tế Xã Song Giang",
   "address": "Xã Song Giang, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0896254,
   "Latitude": 106.1862946
 },
 {
   "STT": 106,
   "Name": "Trạm y tế Xã Bình Dương",
   "address": "Xã Bình Dương, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0781693,
   "Latitude": 106.2580983
 },
 {
   "STT": 107,
   "Name": "Trạm y tế Xã Lãng Ngâm",
   "address": "Xã Lãng Ngâm, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0713692,
   "Latitude": 106.134834
 },
 {
   "STT": 108,
   "Name": "Trạm y tế Xã Nhân Thắng",
   "address": "Xã Nhân Thắng, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0688969,
   "Latitude": 106.2345633
 },
 {
   "STT": 109,
   "Name": "Trạm y tế Xã Xuân Lai",
   "address": "Xã Xuân Lai, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0562091,
   "Latitude": 106.1909027
 },
 {
   "STT": 110,
   "Name": "Trạm y tế Xã Đông Cứu",
   "address": "Xã Đông Cứu, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0540878,
   "Latitude": 106.1786787
 },
 {
   "STT": 111,
   "Name": "Trạm y tế Xã Đại Bái",
   "address": "Xã Đại Bái, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0480922,
   "Latitude": 106.1428645
 },
 {
   "STT": 112,
   "Name": "Trạm y tế Xã Quỳnh Phú",
   "address": "Xã Quỳnh Phú, Huyện Gia Bình, Tỉnh Bắc Ninh",
   "Longtitude": 21.0309357,
   "Latitude": 106.1786787
 },
 {
   "STT": 113,
   "Name": "Trạm y tế Thị trấn Thứa",
   "address": "Thị trấn Thứa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0222876,
   "Latitude": 106.2080897
 },
 {
   "STT": 114,
   "Name": "Trạm y tế Xã An Thịnh",
   "address": "Xã An Thịnh, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0618934,
   "Latitude": 106.2795902
 },
 {
   "STT": 115,
   "Name": "Trạm y tế Xã Trung Kênh",
   "address": "Xã Trung Kênh, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0417946,
   "Latitude": 106.2881384
 },
 {
   "STT": 116,
   "Name": "Trạm y tế Xã Phú Hòa",
   "address": "Xã Phú Hòa, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0396792,
   "Latitude": 106.2373363
 },
 {
   "STT": 117,
   "Name": "Trạm y tế Xã Mỹ Hương",
   "address": "Xã Mỹ Hương, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0344838,
   "Latitude": 106.2610404
 },
 {
   "STT": 118,
   "Name": "Trạm y tế Xã Tân Lãng",
   "address": "Xã Tân Lãng, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.029792,
   "Latitude": 106.1904426
 },
 {
   "STT": 119,
   "Name": "Trạm y tế Xã Quảng Phú",
   "address": "Xã Quảng Phú, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0178933,
   "Latitude": 106.1639749
 },
 {
   "STT": 120,
   "Name": "Trạm y tế Xã Trừng Xá",
   "address": "Xã Trừng Xá, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0278149,
   "Latitude": 106.2404468
 },
 {
   "STT": 121,
   "Name": "Trạm y tế Xã Lai Hạ",
   "address": "Xã Lai Hạ, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0223879,
   "Latitude": 106.2739954
 },
 {
   "STT": 122,
   "Name": "Trạm y tế Xã Trung Chính",
   "address": "Xã Trung Chính, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0105199,
   "Latitude": 106.227617
 },
 {
   "STT": 123,
   "Name": "Trạm y tế Xã Minh Tân",
   "address": "Xã Minh Tân, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0030294,
   "Latitude": 106.266953
 },
 {
   "STT": 124,
   "Name": "Trạm y tế Xã Bình Định",
   "address": "Xã Bình Định, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 20.998473,
   "Latitude": 106.169762
 },
 {
   "STT": 125,
   "Name": "Trạm y tế Xã Phú Lương",
   "address": "Xã Phú Lương, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 21.0009245,
   "Latitude": 106.2059233
 },
 {
   "STT": 126,
   "Name": "Trạm y tế Xã Lâm Thao",
   "address": "Xã Lâm Thao, Huyện Lương Tài, Tỉnh Bắc Ninh",
   "Longtitude": 20.9848167,
   "Latitude": 106.1835369
 }
];