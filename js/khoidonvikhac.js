var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Trung tâm Truyền thông giáo dục sức khỏe Bạc Liêu",
   "area": "Tỉnh",
   "address": "Vũ Ninh, Bắc Ninh, Việt Nam",
   "Longtitude": 21.1911092,
   "Latitude": 106.0774505
 },
 {
   "STT": 2,
   "Name": "Trung tâm kiểm soát bệnh tật Bắc Ninh",
   "area": "Tỉnh",
   "address": "181 Lý Thái Tông, Đại Phúc, Bắc Ninh, Việt Nam",
   "Longtitude": 21.1911089,
   "Latitude": 106.0708844
 },
 {
   "STT": 3,
   "Name": "Trung tâm Kiểm nghiệm Thuốc, Mỹ phẩm và Thực phẩm Bạc Liêu",
   "area": "Tỉnh",
   "address": "Vũ Ninh, Thành phố Bắc Ninh, Bắc Ninh, Việt Nam",
   "Longtitude": 21.1909217,
   "Latitude": 106.0776192
 },
 {
   "STT": 5,
   "Name": "Trung tâm Phòng, chống HIV/AIDS Bắc Ninh",
   "area": "Tỉnh",
   "address": "Huyền Quang, Đại Phúc, Bắc Ninh, Việt Nam",
   "Longtitude": 21.170306,
   "Latitude": 106.065336
 },
 {
   "STT": 6,
   "Name": "Trung tâm Chăm sóc sức khỏe sinh sản Bắc Ninh",
   "area": "Tỉnh",
   "address": "Đường Vũ Ninh, Phường Vũ Ninh, Vũ Ninh, Bắc Ninh, Việt Nam",
   "Longtitude": 21.190274,
   "Latitude": 106.0778153
 },
 {
   "STT": 7,
   "Name": "Trung tâm Pháp y Bắc Ninh",
   "area": "Tỉnh",
   "address": "Phường Võ Cường, Thành Phố Bắc Ninh, Tỉnh Bắc Ninh",
   "Longtitude": 21.1669241,
   "Latitude": 106.0671508
 },
 {
   "STT": 9,
   "Name": "Chi cục Dân số - Kế hoạch hóa gia đình Bắc Ninh",
   "area": "Tỉnh",
   "address": "215 Ngô Gia Tự, Suối Hoa, Bắc Ninh, Việt Nam",
   "Longtitude": 21.1885611,
   "Latitude": 106.0706419
 },
 {
   "STT": 10,
   "Name": "Chi cục An toàn vệ sinh thực phẩm",
   "area": "Tỉnh",
   "address": "Đường Bình Than, Võ Cường, Bắc Ninh",
   "Longtitude": 21.167141,
   "Latitude": 106.0629713
 }
];